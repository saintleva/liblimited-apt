#
# Copyright (C) Anton Liaukevich 2011-2015 <leva.dev@gmail.com>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#


YOU_HAVE_NOT_PRIVILEGES = 10
ATTEMPT_TO_PERFORM_SYSTEM_COMPOSING = 11
YOU_ARE_NOT_COOWNER_OF_PACKAGE = 12
GROUP_NOT_EXIST = 20
PRIVILEGED_SCRIPT_HAS_BEEN_RUN_INCORRECTLY = 21
ERROR_WHILE_PARSING_CONFIG_FILES = 30
STUB = 100